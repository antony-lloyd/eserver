﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace eServer
{
    public class eServerClient
    {
        TcpClient client;
        NetworkStream stream, sendStream;
        ASCIIEncoding enc;

        private int msgSize;
        private string[] configFile, configLine;
        private byte[] msg;
        private string serverData;
        private bool consoleOutput;

        private Thread RecieveThread;

        // Callback to get server data
        public delegate void GetData(object data);
        //

        public eServerClient(bool consoleOutput = true)
        {
            client = new TcpClient();
            enc = new ASCIIEncoding();
            this.consoleOutput = consoleOutput;

            msgSize = 256;
        }

        private void ClientOutput(string txt)
        {
            if(consoleOutput)
                Console.Write(">> {0}", txt);
        }

        public void ReadConfig(string path)
        {
            ClientOutput("Reading config file\n");

            if (!File.Exists(path))
            {
                ClientOutput("The config file does not exist\n");
                MessageBox.Show("The config file does not exist!", "eServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            configFile = File.ReadAllLines(path);

            foreach (string line in configFile)
            {
                configLine = line.Split(' ');

                switch (configLine[0])
                {
                    case "msg":
                        msgSize = int.Parse(configLine[1]);
                        break;
                }
            }
        }

        public void Connect(string name, IPAddress ip, int port, GetData dataOutput)
        {
            ClientOutput("Connecting to server\n");

            client.Connect(ip, port);

            RecieveThread = new Thread(new ParameterizedThreadStart(ReadData));
            RecieveThread.Start(new object[1] { dataOutput });

            SendData(name + " " + ip.ToString());
        }

        public void Disconnect()
        {
            ClientOutput("Disconnecting from server");

            RecieveThread.Abort();
            client.Close();
        }

        public void SendData(string data)
        {
            sendStream = client.GetStream();
            byte[] sendData = enc.GetBytes(data);
            sendStream.Write(sendData, 0, sendData.Length);
            sendStream.Flush();
        }

        private void ReadData(object args)
        {
            Array argsV = (Array)args;
            GetData callback = (GetData)argsV.GetValue(0);

            msg = new byte[msgSize];

            while (true)
            {
                stream = client.GetStream();
                stream.Read(msg, 0, msg.Length);

                serverData = enc.GetString(msg);
                callback(serverData);
            }
        }
    }
}
