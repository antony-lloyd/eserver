﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace eServer
{
    public class eServerListener
    {
        public struct Client
        {
            public string name;
            public IPAddress ipAddress;
            public TcpClient client;
        };

        public delegate void ConsoleOutput(string text);
        private ConsoleOutput output;

        private ASCIIEncoding enc;

        private IPAddress IpAdd;
        private int Port, MessageSize;

        public TcpListener server;
        private Thread ClientListen;

        private SortedList<string, Client> ClientList;
        private List<IPAddress> BlockIps;
        private List<Thread> ClientThreads;

        private Client newClientInfo;

        private string[] configFile, configLine, configBlockIp;

        // Callback function from app code when the server recieves data
        public delegate void GetClientData(object data);
        // Callback function from app code when a client connects
        public delegate void GetClientInfo(object data);
        //

        public eServerListener(ConsoleOutput output = null)
        {
            this.output = output;

            enc = new ASCIIEncoding();

            ServerOutput("*********************************\neServer v0.1\nCreated by: Antony Lloyd\nwww.only-antony.com\n*********************************\n");
        }

        private void ServerOutput(string txt)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            if (output == null)
                Console.WriteLine(">> {0}", txt);
            else
                output(">> " + txt);
        }

        public void Setup(IPAddress ip, int port)
        {
            ServerOutput("Setting up server\n");

            IpAdd = ip;
            Port = port;
            MessageSize = 256;

            ClientList = new SortedList<string, Client>();
            BlockIps = new List<IPAddress>();
            ClientThreads = new List<Thread>();

            server = new TcpListener(IpAdd, Port);
        }

        public bool SetupConfig(string path)
        {
            ServerOutput("Setting up server from config\n");

            if (!File.Exists(path))
            {
                ServerOutput("Server config does not exist!\n");
                MessageBox.Show("Server config does not exist!", "eServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            configFile = File.ReadAllLines(path);
            foreach (string line in configFile)
            {
                configLine = line.Split(' ');
                switch (configLine[0])
                {
                    case "ip":
                        IpAdd = IPAddress.Parse(configLine[1]);
                        break;
                    case "port":
                        Port = int.Parse(configLine[1]);
                        break;
                    case "msg":
                        MessageSize = int.Parse(configLine[1]);
                        break;
                    case "block":
                        configBlockIp = configLine[1].Split(';');
                        for (int i = 0; i < configBlockIp.Length; i++)
                            BlockIps.Add(IPAddress.Parse(configBlockIp[i]));
                        break;
                }
            }

            ClientList = new SortedList<string, Client>();
            BlockIps = new List<IPAddress>();
            ClientThreads = new List<Thread>();

            server = new TcpListener(IpAdd, Port);

            ServerOutput("Server configured\n");

            return true;
        }

        public void Start(GetClientData callback, GetClientInfo callbackInfo)
        {
            ClientListen = new Thread(new ParameterizedThreadStart(ListenForClients));

            server.Start();

            ServerOutput("The server is now running.\n\tIP: " + IpAdd + "\tPort: " + Port + "\n");

            ClientListen.Start(new object[2]{ callback, callbackInfo });
        }

        public void Stop()
        {
            foreach (Thread clientThread in ClientThreads)
                clientThread.Abort();

            foreach (Client client in ClientList.Values)
                DisconnectClient(client);
        }

        public void DisconnectClient(Client client)
        {
            client.client.Close();
            ClientList.Remove(client.name);
        }

        private void ListenForClients(object args)
        {
            Array argsV = (Array)args;
            GetClientData callback = (GetClientData)argsV.GetValue(0);
            GetClientInfo callbackInfo = (GetClientInfo)argsV.GetValue(1);

            ServerOutput("Listening for clients\n");

            while (true)
            {
                TcpClient newClient = server.AcceptTcpClient();
                IPAddress clientIp = (newClient.Client.RemoteEndPoint as IPEndPoint).Address;

                ServerOutput("Reciving connection from " + clientIp + "\n");

                for(int i = 0; i < BlockIps.Count; i++)
                    if (BlockIps[i] == clientIp)
                    {
                        ServerOutput(clientIp + " is on the block list. Connection refused.\n");
                        break;
                    }

                newClientInfo = new Client();
                newClientInfo.client = newClient;
                newClientInfo.ipAddress = clientIp;
                newClientInfo.name = clientIp.ToString();
                ClientList.Add(clientIp.ToString(), newClientInfo);

                ServerOutput("Connection accepted from " + clientIp + "\n");

                callbackInfo(new object[1] { newClientInfo.ipAddress });

                Thread clientThread = new Thread(new ParameterizedThreadStart(ClientCommunication));
                clientThread.Start(new object[3] { newClientInfo, callback, callbackInfo });

                ClientThreads.Add(clientThread);
            }
        }

        private void ClientCommunication(object args)
        {
            Array argsV = (Array)args;
            Client clientInfo = (Client)argsV.GetValue(0);
            GetClientData callback = (GetClientData)argsV.GetValue(1);
            GetClientInfo callbackInfo = (GetClientInfo)argsV.GetValue(2);

            ASCIIEncoding enc = new ASCIIEncoding();

            NetworkStream stream = clientInfo.client.GetStream();
            byte[] message = new byte[MessageSize];
            int bytesRead;
            bool GotClientInfo = false;
            string[] clientInfoArr;
            Client tmpClient;

            string data;

            while (true)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = stream.Read(message, 0, message.Length);
                }
                catch { break; }

                if (bytesRead == 0) break;

                data = enc.GetString(message, 0, message.Length);

                if (!GotClientInfo)
                {
                    clientInfoArr = data.Split(' ');
                    tmpClient = ClientList[clientInfo.name];
                    tmpClient.name = clientInfoArr[0];
                    ClientList[clientInfo.name] = (Client)tmpClient;
                    clientInfo.name = clientInfoArr[0];

                    callbackInfo(new object[2] { clientInfoArr[0], clientInfo.ipAddress });
                    ServerOutput("The client " + clientInfo.ipAddress + " has the name " + clientInfo.name);

                    GotClientInfo = true;
                }
                else
                    callback(new object[2] { clientInfo.name, (string)data });
            }

            ServerOutput("Client " + clientInfo.ipAddress + " disconnecting");

            clientInfo.client.Close();
            ClientList.Remove(clientInfo.name);

            Thread.CurrentThread.Abort();
        }

        public void SendData(string data, Client client)
        {
            NetworkStream stream;

            stream = client.client.GetStream();
            byte[] buffer = enc.GetBytes(data);

            stream.Write(buffer, 0, buffer.Length);
            stream.Flush();
        }

        public Client GetClient(string name)
        {
            return ClientList[name];
        }

        public Client GetClient(int index)
        {
            return ClientList.Values[index];
        }

        public int ClientCount()
        {
            return ClientList.Count;
        }
    }
}
